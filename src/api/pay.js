import axios from 'axios'

// 配置
axios.defaults.baseURL = "http://kg.zhaodashen.cn/v1/"


axios.interceptors.request.use(function(config){
    config.headers['token'] = localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
    //在发送请求前做什么
    return config;
},function(error){
    //对请求错误做些什么
    return Promise.reject(error);
})


export const getPayApi = params =>{
    return axios.get('order/index.jsp',{params}).then(res=>res.data)
}