import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/iconfont/iconfont.css'


// 配置全局文件&样式
import '@/utils/rem.js'  
import '@/utils/filters.js'  
import '@/assets/css/reset.scss'  
// 配置全局文件&样式


// 配置vant
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);
//配置vant

//配置vant-icon
import { Icon } from 'vant';
Vue.use(Icon);
//配置vant-icon


//配置单选框
import { RadioGroup, Radio } from 'vant';
Vue.use(Radio);
Vue.use(RadioGroup);
//配置单选框


// 导航
import { NavBar } from 'vant';
Vue.use(NavBar);
// 导航

// 按钮
import { Button } from 'vant';
Vue.use(Button);
// 按钮

// 遮罩层
import { Overlay } from 'vant';

Vue.use(Overlay);
// 遮罩层

Vue.config.productionTip = false
// 配置全局文件&样式
import '@/utils/rem.js'
import '@/utils/filters.js'
import '@/assets/css/reset.scss'

// 全局引入vant的提示框
import { Toast } from "vant";
Vue.use(Toast);
//Vant
import Vant from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
